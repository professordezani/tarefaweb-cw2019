using Microsoft.AspNetCore.Mvc;
using TarefaWeb.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace TarefaWeb.Controllers
{
    public class TarefaController: Controller
    {
        private static List<Tarefa> lista = new List<Tarefa>();

        public TarefaController()
        {
            if(lista.Count == 0)
            {
                Tarefa t1 = new Tarefa();
                t1.Nome = "Lavar o carro" ;
                t1.Concluida = false;

                Tarefa t2 = new Tarefa();
                t2.Nome = "Passear com o cachorro";
                t2.Concluida = true;
                
                lista.Add(t1);
                lista.Add(t2);
            }
        }

        public IActionResult Index()
        {
            // retornar um .cshtml com o mesmo nome do método
            // return Views/Tarefa/Index.cshtml

            // View recebe como parâmetro o modelo a ser tratado no HTML
            return View(lista);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Tarefa t)
        {
            lista.Add(t);
            
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Search(IFormCollection form)
        {
            // using Microsoft.AspNetCore.Http; --> IFormCollection

            string palavra = form["palavra"]; // <input name="palavra"..

            // List<Tarefa> listaAux = new List<Tarefa>();
            // foreach(Tarefa t in lista)
            // {
            //     if(t.Nome.ToLower().Contains(palavra.ToLower()))
            //     {
            //         listaAux.Add(t);
            //     }
            // }

            // using System.Linq;
            // IEnumerable<Tarefa> listaAux = from t in lista select t;
            // var listaAux = from t in lista
            //                 where t.Nome.ToLower().Contains(palavra.ToLower())
            //                 select t;

            var listaAux = lista.Where(t => t.Nome.Contains(palavra));

            return View("Index", listaAux.ToList());
        }
    }
}