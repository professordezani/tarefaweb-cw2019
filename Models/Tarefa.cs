namespace TarefaWeb.Models
{
    public class Tarefa
    {
        // A partir da versão 3 do C#
        // Propriedade automática:
        public string Nome {get; set;}
        public bool Concluida { get; set; }

        // prop

        // campo (field)
        // private string nome;

        // propriedade:
        // public string Nome 
        // {
        //     get
        //     {
        //         return nome;
        //     }
        //     set
        //     {
        //         Nome = value;
        //     }
        // }
    }
}

// Tarefa t = new Tarefa();
// t.setNome("Tarefa 1"); // Java RUIM!
// t.Nome = "Tarefa 1"; // C# set  BOM!
// string x = t.Nome; // C# get BOM!